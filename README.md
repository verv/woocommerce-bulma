# Bulma for Woocommerce

This is a series of Woocommerce templates that have been modified to use [Bulma](https://bulma.io/) markup, instead of the default WooCoomerce markup.

While all the templates from WooCommerce are included, only the bare minimum have been modified for use in a theme. If you've spotted something I've missed, please raise an issue or PR.

## How do I use this?
Include this as a git submodule in your Wordpress theme (or clone and copy manually).

To use as a submodule, run the following from the _root_ of your theme directory:

```
git submodule add https://bitbucket.org/verv/woocommerce-bulma.git woocommerce/
```

To update the submodule, run the following:

```
cd woocommerce && git pull
```

Note that when updating you'll need to merge any conflicts between the module version and your copy (but this is why a submodule works best here).

## Using Timber?
If you're using Timber, you'll need a very basic `page-plugin.twig` file, which acts as the container for WooCommerce (and presumably, other plugins):

```
{% block content %}
  {{content}}
{% endblock %}
```

## Troubleshooting:
You should ensure that:
* Your theme doesn't include a `woocommerce.php` file (see [this page](https://docs.woocommerce.com/document/template-structure/))
* The variable `WC_TEMPLATE_DEBUG_MODE` is not set to true
* Your theme declares WooCommerce theme support, using `add_theme_support`

## Recommendations:

### WooCommerce Styling
You may wish to remove the WooCommerce Stylesheet just incase there's no additional styling added. Add the following to `functions.php`:

```
add_filter('woocommerce_enqueue_styles', '__return_empty_array');
```

### WooCommerce Notices
Because WooCommerce adds notices to the DOM on the fly, the [traditional JavaScript for handling the delete/close button](https://bulma.io/documentation/elements/notification/#javascript-example) in Bulma's notifications won't work. To counter this, you'll need to make use of a [MutationObserver](https://developer.mozilla.org/en-US/docs/Web/API/MutationObserver). An easy to use implementation I've worked with is [arrive.js](https://github.com/uzairfarooq/arrive). Using `arrive.js`, it becomes as simple as:

```
document.arrive('.notification .delete', function() {
    var notification = this.parentNode;
    this.addEventListener('click', () => {
        notification.parentNode.removeChild(notification);
    });
});
```

## Version Information:
* Bulma Version: 0.9.1
* WooCommerce: 4.7.1
