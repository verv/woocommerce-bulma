<?php

/**
 * Login Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-login.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 4.1.0
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

do_action('woocommerce_before_customer_login_form'); ?>
<section class="section px-0">
    <h2><?php esc_html_e('Login', 'woocommerce'); ?></h2>
    <p><a href="<?php echo esc_url(wp_lostpassword_url()); ?>"><?php esc_html_e('Lost your password?', 'woocommerce'); ?></a></p>
    <form action="" method="post">
        <?php do_action('woocommerce_login_form_start'); ?>
        <div class="control">
            <label for="username" class="label"><?php esc_html_e('Username or email address', 'woocommerce'); ?>&nbsp;<span class="has-text-danger">*</span></label>
            <div class="control">
                <input type="text" name="username" id="username" autocomplete="username" value="<?php echo (!empty($_POST['username'])) ? esc_attr(wp_unslash($_POST['username'])) : ''; ?>" class="input">
            </div>
        </div>
        <div class="control">
            <label for="password" class="label"><?php esc_html_e('Password', 'woocommerce'); ?>&nbsp;<span class="has-text-danger">*</span></label>
            <div class="control">
                <input type="password" name="password" id="password" class="input" autocomplete="current-password">
            </div>
        </div>
        <?php do_action('woocommerce_login_form'); ?>
        <div class="field">
            <div class="control">
                <label class="checkbox">
                    <input type="checkbox" name="remberme" id="rememberme" value="forever">
                    <?php esc_html_e('Remember me', 'woocommerce'); ?>
                </label>
            </div>
        </div>
        <div class="is-grouped">
            <div class="control">
                <?php wp_nonce_field('woocommerce-login', 'woocommerce-login-nonce'); ?>
                <button type="submit" class="button is-link" name="login" value="<?php esc_attr_e('Log in', 'woocommerce'); ?>"><?php esc_html_e('Log in', 'woocommerce'); ?></button>
            </div>
        </div>
    </form>
    <?php do_action('woocommerce_login_form_end'); ?>
</section>

<?php if ('yes' === get_option('woocommerce_enable_myaccount_registration')) : ?>
    <section class="section px-0">
        <h2><?php esc_html_e('Register', 'woocommerce'); ?></h2>
        <form action="" method="post">
            <?php do_action('woocommerce_register_form_start'); ?>
            <?php if ('no' === get_option('woocommerce_registration_generate_username')) : ?>
                <div class="field">
                    <label for="reg_username" class="label"><?php esc_html_e('Username', 'woocommerce'); ?>&nbsp;<span class="has-text-danger">*</span></label>
                    <div class="control">
                        <input type="text" name="reg_username" id="reg_username" class="input" autocomplete="username" value="<?php echo (!empty($_POST['username'])) ? esc_attr(wp_unslash($_POST['username'])) : ''; ?>">
                    </div>
                </div>
            <?php endif; ?>
            <div class="field">
                <label for="reg_email" class="label"><?php esc_html_e('Email address', 'woocommerce'); ?>&nbsp;<span class="has-text-danger">*</span></label>
                <div class="control">
                    <input type="email" name="reg_email" id="reg_email" class="input" autocomplete="email" value="<?php echo (!empty($_POST['email'])) ? esc_attr(wp_unslash($_POST['email'])) : ''; ?>">
                </div>
            </div>
            <?php if ('no' === get_option('woocommerce_registration_generate_password')) : ?>
                <div class="field">
                    <label for="reg_password" class="label"><?php esc_html_e('Password', 'woocommerce'); ?>&nbsp;<span class="has-text-danger">*</span></label>
                    <div class="control">
                        <input type="password" name="reg_password" id="reg_password" class="input" autocomplete="email" value="<?php echo (!empty($_POST['email'])) ? esc_attr(wp_unslash($_POST['email'])) : ''; ?>">
                    </div>
                </div>
            <?php else : ?>
                <div class="field">
                    <p><?php esc_html_e('A password will be sent to your email address.', 'woocommerce'); ?></p>
                </div>
            <?php endif; ?>
            <?php do_action('woocommerce_register_form'); ?>
            <div class="field is-grouped">
                <?php wp_nonce_field('woocommerce-register', 'woocommerce-register-nonce'); ?>
                <button type="submit" class="button is-link" name="register" value="<?php esc_attr_e('Register', 'woocommerce'); ?>"><?php esc_html_e('Register', 'woocommerce'); ?></button>
            </div>
            <?php do_action('woocommerce_register_form_end'); ?>
        </form>
    </section>
<?php endif; ?>
<?php do_action('woocommerce_after_customer_login_form'); ?>
